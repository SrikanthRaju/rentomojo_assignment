//
//  DatabaseConfigration.swift
//  Rentomojo
//
//  Created by Srikanth on 17/02/18.
//  Copyright © 2018 Assignment. All rights reserved.
//

import Foundation
import RealmSwift

struct DB_Configration {
    
    static let SchemaVersion: UInt64 = 1
    static let Default_Name  = "Rentomojo.realm"
}


struct Database {
    
    private init() {}
    
    static func InitializeDatabase() {
        
        var config = Realm.Configuration()
        
        // Use the default directory, but replace the filename with the username
        config.fileURL = config.fileURL!.deletingLastPathComponent().appendingPathComponent(DB_Configration.Default_Name)
        config.schemaVersion = DB_Configration.SchemaVersion
        Realm.Configuration.defaultConfiguration = config
        
        print("DB Path:-   ",config.fileURL!)
    }

}

