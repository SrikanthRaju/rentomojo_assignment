//
//  Furniture.swift
//  Rentomojo
//
//  Created by Srikanth on 17/02/18.
//  Copyright © 2018 Assignment. All rights reserved.
//

import Foundation
import RealmSwift

class Furniture: Object {
    
    @objc dynamic var id = UUID().uuidString
    @objc dynamic var name = ""
    @objc dynamic var price = ""
    @objc dynamic var imageName = ""

    override static func primaryKey() -> String? {
        return "id"
    }
    
    
}
