//
//  FurnituresList_ViewController.swift
//  Rentomojo
//
//  Created by Srikanth on 17/02/18.
//  Copyright © 2018 Assignment. All rights reserved.
//

import UIKit
import RealmSwift

class FurnituresList_ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var realm: Realm!
    
    
    var furnituresList = [Results<Furniture>]()
    var notificationToken: NotificationToken?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        do {
            realm = try Realm()
        } catch {
            print(error)
        }
        
        tableView.register(UINib(nibName: "Furniture_Info_Cell", bundle: nil), forCellReuseIdentifier: "item_cell_identifier")
        
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableViewAutomaticDimension
        
        let objects = realm.objects(Furniture.self)
        furnituresList.append(objects)
        
        notificationToken = objects.observe({ [unowned self] (changes) in

            switch changes {

            case .initial: break

            case .update( _, let deletions, let insertions, let updates):
                let fromRow = { (row: Int) in return IndexPath(row: row, section: 0) }

                self.tableView.beginUpdates()
                if insertions.count > 0 {
                    self.tableView.insertRows(at: insertions.map(fromRow), with: .none)
                }
                if updates.count > 0 {
                    self.tableView.reloadRows(at: updates.map(fromRow), with: .automatic)
                }

                if deletions.count > 0 {
                    self.tableView.deleteRows(at: deletions.map(fromRow), with: .automatic)
                }
                self.tableView.endUpdates()


            case .error(let error):
                fatalError("\(error)")

            }
        })
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        ImageCache.default.clearAll()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func addBulkEntries() {
        
        let alert = UIAlertController(title: "Alert !!!", message: "You are going to dupicate the items that are available in list", preferredStyle: .alert)
        
        let duplicateAction = UIAlertAction(title: "Duplicate", style: UIAlertActionStyle.default) { [unowned self] action in
            self.duplicateList()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)

        alert.addAction(duplicateAction)
        alert.addAction(cancelAction)

        self.present(alert, animated: true, completion: nil)
    }
    
    private func duplicateList() {
        
        DispatchQueue.global(qos: .background).async {
            // Get new realm and table since we are in a new thread
            let realm = try! Realm()
            let objects = realm.objects(Furniture.self)
            
            realm.beginWrite()
            
            for obj in objects {
                
                autoreleasepool {
                    
                    let fur = Furniture()
                    fur.imageName = obj.imageName
                    fur.name = obj.name
                    fur.price = obj.price
                    
                    realm.add(fur)
                }
            }
            try! realm.commitWrite()
        }
    
    }
    
    
    
    @IBAction func unwindToFurnitureList(_ segue: UIStoryboardSegue) {
        
//        if segue.identifier == "SaveUnwindAction" {
//            tableView.reloadData()
//        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let nvc = segue.destination as? UINavigationController, let vc = nvc.topViewController as? AddFurniture_ViewController else {
            return
        }
        vc.realm = try? Realm()
        
    }
}

extension FurnituresList_ViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return furnituresList[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "item_cell_identifier", for: indexPath) as! Furniture_Info_Cell
        
        let furniture = furnituresList[indexPath.section][indexPath.row]
        cell.setFurniture(info: furniture)
        
        return cell
    }
    
}
