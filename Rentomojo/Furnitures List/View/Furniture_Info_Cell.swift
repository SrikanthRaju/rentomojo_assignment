//
//  Furniture_Info_Cell.swift
//  Rentomojo
//
//  Created by Srikanth on 17/02/18.
//  Copyright © 2018 Assignment. All rights reserved.
//

import UIKit

class Furniture_Info_Cell: UITableViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        imgView.layer.borderColor = UIColor.lightGray.cgColor
        imgView.layer.borderWidth = 0.2
        imgView.layer.cornerRadius = 3.0
        
    }

    
    func setFurniture(info: Furniture) {
        
        priceLabel.text = "₹ " + info.price
        nameLabel.text = info.name
        let imgname = info.imageName
        
        guard let image = ImageCache.default.image(with: imgname) else {
            loadImageAsync(imgname)
            return
        }
        
        self.imgView.image = image

    }
    
    func loadImageAsync(_ imageName: String) {
        
        DispatchQueue.global().async {
            
            guard let imagePath = FolderManager.path(to: imageName),
                let image = UIImage(contentsOfFile: imagePath) else {
                    return
            }
            
            ImageCache.default.setImage(image, for: imageName)
            
            DispatchQueue.main.async {
                self.imgView.image = image
            }
        }
        
        
    }
    

}


