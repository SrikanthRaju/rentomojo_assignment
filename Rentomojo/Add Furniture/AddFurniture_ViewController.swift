//
//  AddFurniture_ViewController.swift
//  Rentomojo
//
//  Created by Srikanth on 17/02/18.
//  Copyright © 2018 Assignment. All rights reserved.
//

import UIKit
import RealmSwift


class AddFurniture_ViewController: UIViewController {

    fileprivate var picker = UIImagePickerController()
    var realm: Realm!
    
    
    
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var priceTextField: UITextField!
    
    @IBOutlet weak var scrollView: UIScrollView!
    fileprivate var activeTextField: UITextField?
    
    fileprivate var newFileName: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var frame = CGRect.zero
        frame.size.width = 32
        frame.size.height = 32
        let leftView = UIView(frame: frame)
       
        frame.size.width = 26
        let label = UILabel(frame: frame)
        label.text = "₹"
        label.textAlignment = .right
        label.font = UIFont.systemFont(ofSize: 18)
        leftView.addSubview(label)
        priceTextField.leftView = leftView
        priceTextField.leftViewMode = .always
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   
    @IBAction fileprivate func btnImagePickerClicked(sender: AnyObject) {
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openCamera()
            
        }
        let gallaryAction = UIAlertAction(title: "Gallary", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
            
        }
        // Add the actions
        picker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        // Present the controller
        self.present(alert, animated: true, completion: nil)
        
    }
    
    fileprivate func openCamera()
    {
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            picker.sourceType = .camera
            self .present(picker, animated: true, completion: nil)
        } else {
            openGallary()
        }

    }
    
    fileprivate func openGallary()
    {
        picker.sourceType = .photoLibrary
        self.present(picker, animated: true, completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
       NotificationCenter.default.removeObserver(self)

    }
    
    @objc fileprivate func keyboardWillShow(_ notification: Notification) {
        
        let userInfo = notification.userInfo
        let keyboardFrame = userInfo?[UIKeyboardFrameEndUserInfoKey] as! CGRect
        let contentInset = UIEdgeInsetsMake(0.0, 0.0, keyboardFrame.height + 20, 0.0)
        scrollView.contentInset = contentInset
        scrollView.scrollIndicatorInsets = contentInset
        scrollView.scrollRectToVisible(activeTextField!.frame, animated: true)
    }
    

    @objc fileprivate func keyboardWillHide(_ notification: Notification) {

        let contentInset = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
        scrollView.scrollIndicatorInsets = contentInset
    }
    @IBAction fileprivate func saveBarButtonAction(_ sender: UIBarButtonItem) {
        
        
        let furniture = Furniture()
        furniture.name = nameTextField.text!
        furniture.price = priceTextField.text!
        furniture.imageName = newFileName
                
        try! realm.write {
            realm.add(furniture)
        }
        
        performSegue(withIdentifier: "SaveUnwindAction", sender: nil)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard segue.identifier != "SaveUnwindAction" else {
            return
        }
        
        deleteSaveFileFromFolder()
        
    }
    
    fileprivate func deleteSaveFileFromFolder() {
        
        DispatchQueue.global().async { [unowned self] in
            FolderManager.removeImage(with: self.newFileName)
        }
        
    }

}



extension AddFurniture_ViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        picker .dismiss(animated: true, completion: nil)
        button.isHidden = true
        
       try? saveImage(info[UIImagePickerControllerOriginalImage] as? UIImage)
        
    }
    
    fileprivate func saveImage(_ img: UIImage?) throws {
        guard let image = img else {
            throw NSError(domain: "Image Saving Failed", code: 400, userInfo: nil)
        }
        imageView.image = image
        self.newFileName = UUID().uuidString + ".png"
        
        DispatchQueue.global().async { [unowned self] in
            try? FolderManager.add(image: image, with: self.newFileName)
        }
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        print("picker cancel.")
    }
}

extension AddFurniture_ViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        guard textField == priceTextField else { return true }

        
        let currentText = textField.text ?? ""
        let newText = (currentText as NSString).replacingCharacters(in: range, with: string)
        
        if string.count == 0 {
            return true
        }
        
        return newText.isNumeric
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        activeTextField = nil
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == nameTextField {
            return priceTextField.becomeFirstResponder()
        }
        
        return textField.resignFirstResponder()
        
    }
    
    
}



