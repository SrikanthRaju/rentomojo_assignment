//
//  UIImage+Extension.swift
//  Rentomojo
//
//  Created by Srikanth on 17/02/18.
//  Copyright © 2018 Assignment. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    
    func resize(to targetSize: CGSize, scale: CGFloat = UIScreen.main.scale) -> UIImage {
        
        let size = self.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: (size.width * heightRatio) * scale, height: (size.height * heightRatio) * scale)
        } else {
            newSize = CGSize(width: (size.width * widthRatio) * scale,  height: (size.height * widthRatio) * scale)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
        
    }
    
}
