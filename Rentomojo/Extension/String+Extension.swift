//
//  String+Extension.swift
//  Rentomojo
//
//  Created by Srikanth on 18/02/18.
//  Copyright © 2018 Assignment. All rights reserved.
//

import Foundation

extension String {
    
    var isNumeric: Bool {
        return !isEmpty && range(of: "[^0-9]", options: .regularExpression) == nil
    }
    
}
