//
//  CacheManager.swift
//  Rentomojo
//
//  Created by Srikanth on 17/02/18.
//  Copyright © 2018 Assignment. All rights reserved.
//

import Foundation
import UIKit

class ImageCache {
    
    static let `default` = ImageCache()
    
    let cache: NSCache<AnyObject, UIImage>!
    private init() {
        cache = NSCache<AnyObject, UIImage>()
    }
    
    func image(with name: String) -> UIImage? {
        
        return cache.object(forKey: name as AnyObject)
    }
    
    func setImage(_ image: UIImage, for key: String) {
        cache.setObject(image, forKey: key as AnyObject)
    }
    
    func clearAll() {
        cache.removeAllObjects()
    }
}
