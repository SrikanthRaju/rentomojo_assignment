//
//  FileManager.swift
//  Rentomojo
//
//  Created by Srikanth on 17/02/18.
//  Copyright © 2018 Assignment. All rights reserved.
//

import Foundation
import UIKit

struct FolderManager {
    
    private init() {}
    
    fileprivate static var imagesDirectoryName = "/Images"
    fileprivate static let fileManager = FileManager.default
    
    fileprivate static func documentFolderPath() -> String {
        return NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    }
    
    fileprivate static func pathToImagesDirectory() -> String? {
        
        var path = documentFolderPath()
        path.append("\(imagesDirectoryName)")
        print("Path -->>  ", path)
        guard fileManager.fileExists(atPath: path) else { return nil }
        
        return path
    }
    
    fileprivate static func createImagesDirectory() {
        
        var path = documentFolderPath()
        path.append("\(imagesDirectoryName)")
        guard !fileManager.fileExists(atPath: path) else { return }
        do {
            try fileManager.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
        } catch {
            print(error)
        }
    }
    
    static var imageDirectory: String = {
        
        var imageDirectory = pathToImagesDirectory()
        
        if imageDirectory == nil {
            createImagesDirectory()
            sleep(UInt32(0.0001))
            imageDirectory = pathToImagesDirectory()
        }
        
        return imageDirectory!
    }()
    
    static func add(image: UIImage, size: CGSize = CGSize(width: 75, height: 75), with name: String) throws {

        let resizedImage = image.resize(to: size)

        guard let data = UIImagePNGRepresentation(resizedImage) else {
         
            throw NSError(domain: "Image saving error", code: -1010, userInfo: nil)
        }
        
        let path = imageDirectory + "/\(name)"
        fileManager.createFile(atPath: path, contents: data, attributes: nil)
       
    }
    
    static func removeImage(with name: String) {
        

        let path = imageDirectory + "/\(name)"
        guard !fileManager.fileExists(atPath: path) else { return }

        removeItem(at: path)
        
    }
    
    static func removeItem(at path: String) {
        
        do {
            try fileManager.removeItem(atPath: path)
        } catch {
            print("error --> \(error)")
        }
        
    }
    
    static func path(to filename: String) -> String? {
        guard let imageDir = pathToImagesDirectory() else {
            return nil
        }
        
        return imageDir + "/\(filename)"
    }
    
}
    

