//
//  LaunchViewController.swift
//  Rentomojo
//
//  Created by Srikanth on 18/02/18.
//  Copyright © 2018 Assignment. All rights reserved.
//

import UIKit
import RealmSwift

class LaunchViewController: UIViewController {

    let realm = try? Realm()

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let objects = realm!.objects(Furniture.self)
        if objects.count == 0 {
        
            let nav = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddFurnitureNavigationController")
            guard let nvc = nav as? UINavigationController,
                let vc = nvc.topViewController as? AddFurniture_ViewController else {
                return
            }
            vc.realm = try? Realm()
            present(nav, animated: false, completion: nil)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        performSegue(withIdentifier: "List", sender: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: false)
    }

}
