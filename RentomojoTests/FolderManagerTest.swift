//
//  FolderManagerTest.swift
//  RentomojoTests
//
//  Created by Srikanth on 18/02/18.
//  Copyright © 2018 Assignment. All rights reserved.
//

import XCTest
@testable import Rentomojo

class FolderManagerTest: XCTestCase {
    
    var checkFolderPath: String!
    
    var newImageName: String! = "xyz.png"
    
    override func setUp() {
        super.setUp()

        checkFolderPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        checkFolderPath.append("/Images")
        
        newImageName = "xyz.png"

        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {

        checkFolderPath = nil
        newImageName = nil
        super.tearDown()
    }
    
    
    
    func testCheckImageFolderExistance() {
        
        print(FileManager.default.fileExists(atPath: checkFolderPath))
        
        XCTAssertTrue(FileManager.default.fileExists(atPath: checkFolderPath))
    }
    
    func testCreateImageFolder() {
        
        //This creates Images folder in Documents Folder
        let _ = FolderManager.imageDirectory
        XCTAssert(FileManager.default.fileExists(atPath: checkFolderPath))
    }
    
    func testAddImageToFolder() {
        
        let image = #imageLiteral(resourceName: "Logo")
        
        do {
            try FolderManager.add(image: image, with: newImageName)
        } catch {
            XCTFail("Failed to add Image to folder")
        }
        
        XCTAssert(FileManager.default.fileExists(atPath: checkFolderPath))

        //check if image exist in folder or not
        let imagePath = checkFolderPath + "/\(newImageName!)"
        XCTAssert(FileManager.default.fileExists(atPath: imagePath))
        
    }
    
    func testRemoveImageToFolder() {
        
         FolderManager.removeImage(with: newImageName)
        
        //check if image exist in folder or not
        let imagePath = checkFolderPath + "/\(newImageName!)"
        XCTAssertFalse(FileManager.default.fileExists(atPath: imagePath))
        
    }
    
    func testRemoveNonExistingImageToFolder() {
        
        FolderManager.removeImage(with: "newImageName.png")
        
        //check if image exist in folder or not
        let imagePath = checkFolderPath + "/\(newImageName)"
        XCTAssertFalse(FileManager.default.fileExists(atPath: checkFolderPath))
        XCTAssertFalse(FileManager.default.fileExists(atPath: imagePath))
        
    }
    
    func testRemoveImageFolder() {
        
        FolderManager.removeItem(at: checkFolderPath)
        
        //check if image exist in folder or not
        XCTAssertFalse(FileManager.default.fileExists(atPath: checkFolderPath))
        
    }
    
    func testPath() {
        
        let realPath = FolderManager.path(to: newImageName!)
        XCTAssertNotNil(realPath)
        
        
        let imagePath = checkFolderPath + "/\(newImageName!)"
        XCTAssert(realPath == imagePath)
    }
}
